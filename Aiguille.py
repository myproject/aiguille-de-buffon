import math

from BasicVector import Vec2
from random import randint as ri

class Aiguille:

    size = 0

    @classmethod
    def CreateRandomAiguilleByFirstPos(cls, pos):
        angle = ri(0, 360)
        pos2 = Vec2(pos.x + cls.size * math.cos(angle), pos.y + cls.size * math.sin(angle))
        return Aiguille(pos, pos2)

    def __init__(self, pos1, pos2):
        self.pos1 = pos1
        self.pos2 = pos2

    def crossALine(self, lines):
        res = False
        for line in lines:
            if (self.pos1.x <= line and self.pos2.x >= line) or (self.pos1.x >= line and self.pos2.x <= line):
                res = True
        return res