import time
import pygame
from BasicVector import Vec2
from Aiguille import Aiguille

WINDOWSIZE = (500, 500)
nbLigne = 10
nbAiguille = 300
a = WINDOWSIZE[0] / nbLigne
Aiguille.size = a / 2

posXLignes = [int(i*a+a/2) for i in range(nbLigne)]

def createAiguille():
    global aiguilles
    aiguilles = [Aiguille.CreateRandomAiguilleByFirstPos(Vec2.randomIntVec(minX=0, maxX=WINDOWSIZE[0], minY=0, maxY=WINDOWSIZE[1])) for i in range(nbAiguille)]

createAiguille()

pygame.init()
fenetre = pygame.display.set_mode(WINDOWSIZE)
pygame.display.set_caption("Aiguille de Buffon")

time_per_frame = 1 / 60
last_time = time.time()
frameCount = 0

run = True
while run:
    delta_time = time.time() - last_time
    while delta_time < time_per_frame:
        delta_time = time.time() - last_time

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

            keys = pygame.key.get_pressed()
            if keys[pygame.K_ESCAPE]:
                run = False
            elif keys[pygame.K_F5]:
                createAiguille()

        fenetre.fill((0,0,0))

        for x in posXLignes:
            pygame.draw.line(fenetre, (255, 255, 255), (x, 0), (x, WINDOWSIZE[1]))
        for aiguille in aiguilles:
            pygame.draw.line(fenetre, (255, 0, 0), aiguille.pos1.getPos(), aiguille.pos2.getPos())

        aiguilleOnLine = sum([1 for i in aiguilles if i.crossALine(posXLignes)])

        if aiguilleOnLine != 0:
            res = str(nbAiguille / aiguilleOnLine)
        else:
            res = ""

        pygame.font.init()
        font = pygame.font.SysFont('Comic Sans MS', 15)
        textsurface = font.render(res, False, (255, 255, 255))
        fenetre.blit(textsurface, (0, 0))

    pygame.display.update()

    last_time = time.time()
    frameCount += 1
